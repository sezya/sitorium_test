# -*- coding: utf-8 -*-
'''
Created on 29 сент. 2013 г.

@author: status
'''
from products.models import StoreResponse

class StoreResponses(object):
    def process_response(self, request, response):
        url = request.path
        status_code = response.status_code
        method = request.method
        store_resp = StoreResponse(url=url, status_code=status_code, 
                                   method=method)
        store_resp.save()
        return response
