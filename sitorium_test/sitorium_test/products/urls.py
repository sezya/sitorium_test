# -*- coding: utf-8 -*-
'''
Created on 26 сент. 2013 г.

@author: status
'''
from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'products.views.frontpage', name="frontpage"),
    url(r'^product/(?P<product_id>\d+)/$', 'products.views.product', name="product"),
    url(r'^product_edit/(?P<product_id>\d+)/$', 'products.views.product_edit', name="product_edit"),
    url(r'^store_responses/$', 'products.views.store_responses', name="store_responses"),
#     url(r'^openid_complete/', openid_complete, name="openid_complete"),
#     url(r'^openid_begin/', openid_begin, name="openid_begin"),
#     url(r'^cabinet/(?P<letter_id>\d*)', cabinet, name="cabinet"),
#     url(r'^logout/', logout, name="logout"),
)