# -*- coding: utf-8 -*-
from django.test import TestCase
from products.models import Product
from django.core import mail
from django.conf import settings

class ProductTest(TestCase):
    def test_singnals_products(self):
        admins = settings.ADMINS
        emails = []
        for admin in admins:
            emails.append(admin[1])
        product = Product(product_name="Test", 
                          description="Test descriotion",
                          color = "#000000",
                          color_second="",
                          wieght="1",
                          height="2",
                          photo="photos/2.jpg",
                          )
        product.save()
        mail.outbox[0].from_email
        self.assertEqual(mail.outbox[0].subject, 'Create product')
        product.save()
        self.assertEqual(mail.outbox[1].subject, 'Save product')
        product.delete()
        self.assertEqual(mail.outbox[2].subject, 'Delete product')