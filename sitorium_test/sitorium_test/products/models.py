from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from datetime import datetime

class Product(models.Model):
    product_name = models.CharField('product name', max_length=100)
    description = models.TextField('description')
    wieght = models.IntegerField('wieght')
    height = models.IntegerField('height')
    color = models.CharField('color', max_length=10)
    color_second = models.CharField('color second', max_length=10, blank=True, null=True)
    photo = ThumbnailerImageField(upload_to='photos', default="")
    
    def __unicode__(self):
        return '%s' % self.product_name
    
    class Meta:
        ordering = ('product_name',)
        verbose_name = 'product'
        verbose_name_plural = 'products'
        
from products import signals as product_signals

class StoreResponse(models.Model):
    url = models.URLField('url')
    method = models.CharField('method', max_length=4)
    status_code = models.IntegerField('status_code')
    datetime = models.DateTimeField(editable=False, default=datetime.now)
    
    class Meta:
        ordering = ("-datetime",) 
        
    def __unicode__(self):
        return "%s" % (self.datetime)