# -*- coding: utf-8 -*-
'''
Created on 27 сент. 2013 г.

@author: status
'''
from django.contrib import admin
from products import models

admin.site.register(models.Product)

class StoreResponseAdmin(admin.ModelAdmin):
    list_display = ["datetime", 'status_code', "method", 'url']

admin.site.register(models.StoreResponse, StoreResponseAdmin)