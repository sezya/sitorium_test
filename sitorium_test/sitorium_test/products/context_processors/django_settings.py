# -*- coding: utf-8 -*-
'''
Created on 26 сент. 2013 г.

@author: status
'''
from django.core.context_processors import request
from django.conf import settings

def django_settings(request):
    return {'settings': settings}