# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Product.color_second'
        db.add_column(u'products_product', 'color_second',
                      self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Product.color_second'
        db.delete_column(u'products_product', 'color_second')


    models = {
        u'products.product': {
            'Meta': {'ordering': "('product_name',)", 'object_name': 'Product'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'color_second': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'height': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100'}),
            'product_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'wieght': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['products']