# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Product.color'
        db.alter_column(u'products_product', 'color', self.gf('django.db.models.fields.CharField')(max_length=10))

    def backwards(self, orm):

        # Changing field 'Product.color'
        db.alter_column(u'products_product', 'color', self.gf('django.db.models.fields.TextField')(max_length=10))

    models = {
        u'products.product': {
            'Meta': {'ordering': "('product_name',)", 'object_name': 'Product'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'height': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'wieght': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['products']