# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StoreResponse'
        db.create_table(u'products_storeresponse', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('method', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('status_code', self.gf('django.db.models.fields.IntegerField')()),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'products', ['StoreResponse'])


    def backwards(self, orm):
        # Deleting model 'StoreResponse'
        db.delete_table(u'products_storeresponse')


    models = {
        u'products.product': {
            'Meta': {'ordering': "('product_name',)", 'object_name': 'Product'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'color_second': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'height': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100'}),
            'product_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'wieght': ('django.db.models.fields.IntegerField', [], {})
        },
        u'products.storeresponse': {
            'Meta': {'ordering': "('-datetime',)", 'object_name': 'StoreResponse'},
            'datetime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'method': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'status_code': ('django.db.models.fields.IntegerField', [], {}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['products']