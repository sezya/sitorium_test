# -*- coding: utf-8 -*-
'''
Created on 27 сент. 2013 г.

@author: status
'''
from django import forms
from products.models import Product
from products.widgets import ColorPicker


class ProductForm(forms.ModelForm):
    product_name = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control center',}))
    wieght = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control center',}))
    height = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control center',}))
    color = forms.CharField(label="Color 1", widget=ColorPicker)
    color_second = forms.CharField(required=False,
                                   label="Color 2", widget=ColorPicker)
    description = forms.CharField(
        widget=forms.Textarea(attrs={'class':'form-control col-lg-6',}))
    
    class Meta:
        model = Product
    
    