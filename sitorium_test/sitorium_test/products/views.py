# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from products.models import Product, StoreResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.contrib.auth.decorators import login_required
from products.forms import ProductForm
from django.http.response import HttpResponse
import json

def frontpage(request):
    template_path = "frontpage.html"
    products = Product.objects.all()
    paginator = Paginator(products, settings.PAGINATOR_COUNT_PRODUCTS_ON_PAGE)
    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    context = {'products': products}
    return render_to_response(template_path, context,
                              context_instance=RequestContext(request))

def product(request, product_id):
    template_path = "product.html"
    product = get_object_or_404(Product, id = product_id)
    context = {'product': product}
    return render_to_response(template_path, context,
                              context_instance=RequestContext(request))

@login_required
def product_edit(request, product_id):
    template_path = "product_edit.html"
    product = get_object_or_404(Product, id = product_id)
    if request.method == "POST":
        form = ProductForm(request.POST, request.FILES, instance=product)
        if form.is_valid():
            form.save()
            product = get_object_or_404(Product, id = product_id)
            filename = product.photo.file.name.split('/')[-1:]
            context = {'success': True,
                       'message': 'Success',
                       'photo_url': product.photo.url,
                       'photo': filename
                       }
            print json.dumps(context)
            return HttpResponse(json.dumps(context))
        else:
            filename = product.photo.file.name.split('/')[-1:]
            errors = ''
            for key_error in form.errors.keys():
                errors += '<div><span>%s: </span><span>%s</span></div>' % (key_error, form.errors[key_error])
            context = {'success': False,
                       'message': errors,#form.errors.values(),
                       'photo_url': product.photo.url,
                       'photo': filename
                       }
            print dir(form.errors)
            print json.dumps(context)
            return HttpResponse(json.dumps(context))
        post = True
    else:
        form = ProductForm(instance=product)
        post = False
    context = {'form': form,
               'post': post,
               'product': product,}
    return render_to_response(template_path, context,
                              context_instance=RequestContext(request))
    
def store_responses(request):
    template_path = "store_responses.html"
    responses = StoreResponse.objects.all()
    paginator = Paginator(responses, settings.PAGINATOR_COUNT_RESPONSES_ON_PAGE)
    page = request.GET.get('page')
    try:
        responses = paginator.page(page)
    except PageNotAnInteger:
        responses = paginator.page(1)
    except EmptyPage:
        responses = paginator.page(paginator.num_pages)
    context = {'responses': responses}
    return render_to_response(template_path, context,
                              context_instance=RequestContext(request))    