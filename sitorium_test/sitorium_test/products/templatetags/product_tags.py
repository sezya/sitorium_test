# -*- coding:utf-8 -*-
'''
Created on 29 сент. 2013 г.

@author: status
'''

from django import template
from products.models import Product
from django.conf import settings

register = template.Library()

@register.inclusion_tag('product_random.html')
def product_random():
    MEDIA_URL = settings.MEDIA_URL
    count = settings.COUNT_RANDOM_PRODUCT
    products = Product.objects.all().order_by('?')[:count]
    context = {'products':products, 
               'MEDIA_URL': MEDIA_URL}
    return context