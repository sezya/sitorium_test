# -*- coding: utf-8 -*-
'''
Created on 28 сент. 2013 г.

@author: status
'''
from django.forms.widgets import Widget, TextInput
from django.template.loader import render_to_string
from django.conf import settings
from django.utils.safestring import mark_safe


class ColorPicker(TextInput):
    class Media:
        css = { 'all' : ('%scolorpicker/css/colorpicker.css' % settings.STATIC_URL,), }
        js = ('colorpicker/js/bootstrap-colorpicker.js',
              'js/colorpicker.js',)
    
    def render(self, name, value, attrs=None):
        output = []
        output = render_to_string('colorpicker_widget.html', 
                                  {'value': value,
                                   'name': name})
        return mark_safe(output)
    