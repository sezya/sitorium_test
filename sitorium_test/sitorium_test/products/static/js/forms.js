(function() {
var bar = $('.progress-bar');
var percent = $('.percent');
var photo_field = $('.photo_field a');
var success = $('.alert-success');
var danger = $('.alert-danger');
var inputs = $(this).find('input');
//$('#submit_form').click(function(){  
	$('#product_form').ajaxForm({
	    dataType: "json",
	    beforeSend: function() {
	        success.removeClass('hide');
	        danger.removeClass('hide');
	        success.hide();
	        danger.hide();
	        success.empty();
	        danger.empty()
	        var percentVal = '0%';
	        bar.width(percentVal);
	        $("input").attr("disabled", "disabled");
	        $("textarea").attr("disabled", "disabled");
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	    },
	    success: function(data) {
	        var percentVal = '100%';
	        bar.width(percentVal)
	        percent.html(percentVal);
	        photo_field.attr('href', data.photo_url);
	        photo_field.html(data.photo);
	        if(data.success){
	            success.html(data.message);
	            success.show();
	            }
	        else{
	            danger.html(data.message);
	            danger.show();
	        };
	    },
		complete: function(data) {
			$("input").removeAttr("disabled", "disabled");
	        $("textarea").removeAttr("disabled", "disabled");
		},
		//resetForm: true,
		
	}); 
//});
})();