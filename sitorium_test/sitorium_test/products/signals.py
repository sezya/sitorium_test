# -*- coding: utf-8 -*-
from django.db.models import signals
from django.core.mail import send_mail
from django.conf import settings
from products.models import Product


admins = settings.ADMINS
emails = []
for admin in admins:
    emails.append(admin[1])

def product_saved(sender, **kwargs):
    if kwargs['created']:
        send_mail('Create product', 
              'Created %s' % kwargs['instance'], 
              'noreply@sitorium_test.ru', 
              emails, 
              fail_silently=False)
    else:
        send_mail('Save product', 
                  'Saved %s' % kwargs['instance'], 
                  'noreply@sitorium_test.ru', 
                  emails, 
                  fail_silently=False)
    
signals.post_save.connect(product_saved, sender=Product)

def product_deleted(sender, **kwargs):
    send_mail('Delete product', 
              'Deleted %s' % kwargs['instance'], 
              'noreply@sitorium_test.ru', 
              emails, 
              fail_silently=False)
    
signals.post_delete.connect(product_deleted, sender=Product)